pragma solidity 0.4.24;
// crear contrato math  

contract Math {
    uint x;
    uint y;
    
    function sum(uint _x, uint _y) public pure returns(uint){
        return _x + _y;
    }
    
    function rest(uint _x, uint _y) public pure returns(uint){
        return _x - _y;
    }
    
    function mult(uint _x, uint _y) public pure returns(uint){
        return _x * _y;
    }
    
    function div(uint _x, uint _y) public pure returns(uint){
        require( _y != 0,"No es posible la division entre cero");
        return _x / _y;
    }
}


// crear contrato calculadora
    // donar
    //checar el saldo del contrato
contract calculadora is Math {
    function donate() payable public returns(string){
        return "Gracias por donar";
    }
    
    function getBalance() public view returns(uint){
        return address(this).balance;
    }
}
