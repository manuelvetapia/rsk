pragma solidity ^0.4.24;

contract GetterSetter
{
    //vars
    string message;
    uint price=1 ether;
    
    constructor() payable public
    {
        message='hola Satoshi';
        //msg.sender='0x....3c'; toma el valor de quien ejecuta el programa primero
    }
    
    //function function setMessage() returns (a,b,d)  //si se regresan mas valores van en parentesis y con comas
    //lo que modifica el estado del mensaje se llama transaccion
    //payable indica que va a costar algo la ejecucion
    function setMessage(string _message) payable public returns (bool)
    {
        //si cumple el require se sigue ejecutando sino se sale del flujo
        require(msg.value==price);
        message=_message;
        //msg.sender='0x....3c'; toma el valor de quien ejecuta el programa
        return true;
    }

//si no se modifica el estado del contracto tenemos que ponerle "view" que es solo una consulta, se le llama mensaje
    function getMessage() view public returns (string)
    {
        
        return message;
    }

}
