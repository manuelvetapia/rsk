// Seguro de vida de un viajero descentralizado
// Si el cliente fallece, se le paga el seguro al beneficiario
// 0. El cliente puede comprar solo 1 seguro de vida por 1 ether.
// 1. La entidad de seguros indica si estabas viajando o no.
// 2. La entidad de gobierno valida que falleciste y, si estabas viajando, se paga el seguro.
pragma solidity ^0.4.24;

contract Assurance{
    // Variables
    address government;
    address travelAgency;
    address contrato = this;
    address owner;
    
    
    
//----->    //creamos un struct con ciertos valores    
    struct Customer{
        address beneficiary;
        bool hasContract;
        bool dead;
        bool paid;
        bool traveling;
    }
    
    
//----->    //creamos un mapa donde a cada address le asignamos un struct
    // lista de asegurados
    mapping(address => Customer) public customers;
    
    // Constructor
    constructor(address _government, address _travelAgency) public{
        government = _government;
        travelAgency = _travelAgency;
        owner = msg.sender;
    }
    
    
    
    
    
    // Modificadores
      // solo el owner puede retirar los fondos
      modifier onlyOwner(){
          require(msg.sender == owner);
          _;
      }
      
      // solo la agencia de viajes puede indicar que estas viajando
      modifier onlyTravelAgency(){
          require(msg.sender == travelAgency);
          _;
      }
      // solo el gobierno puede indicar que efectivamente falleciste
      modifier onlyGovernment(){
          require(msg.sender == government);
          _;
      }
    
    
    // Metodos
      // Funcion 1 = comprar contrato de seguro
      function buyContract(address _beneficiary) public payable returns (bool){
          
//----->  creamos un struct nuevo llamado cliente y seteamos el mapping al address actual              
          Customer storage cliente = customers[msg.sender];
          
          // validar que el usuario manda 1 ether
          require(msg.value == 1 ether);
          
          // que no tenga mas de un contrato
          require(!cliente.hasContract);
          
          // que el cliente no sea el mismo que el beneficiario
          require(msg.sender != _beneficiary);
          
          cliente.hasContract = true;
          cliente.beneficiary = _beneficiary;
          cliente.dead = false;
          cliente.paid = false;
          cliente.traveling = false;
          
          return true;
      }
      
      // funcion 2 - declarar que esta viajando
      function setTraveling(address _traveler) public onlyTravelAgency() returns (bool){
          Customer storage traveler = customers[_traveler];
          
          require(traveler.hasContract);
          require(!traveler.dead);
          
          traveler.traveling = true;
          
          return true;
      }
      
      // funcion 3- declarar que fallecio
      function notifyDead(address _traveler) public onlyGovernment() returns (bool){
          Customer storage traveler = customers[_traveler];
          
          // validamos si tiene contrato e iba viajando
          require(traveler.hasContract);
          require(traveler.traveling);
          
          // notificamos defuncion
          traveler.dead = true;
          
          // pagamos
          payment(traveler.beneficiary);
          
          traveler.paid = true;
          traveler.traveling = false;
          
          return true;
      }
      // funcion 4 -pagar al beneficiario
      function payment(address _beneficiary) private {
          _beneficiary.transfer(2 ether);
      }
      
      // funcion 5 -retirar el dinero
      function cashOut() onlyOwner() public payable{
          owner.transfer(contrato.balance);
      }
    
}
