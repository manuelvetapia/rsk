[200~pragma solidity ^0.4.24;
// Contrato de votaciones

contract Votaciones
{
    // 500 votos en total, 100 cada quien
//Declaracion de variables
    //limite de votos emitidos
    uint256 totalVotos=500;
    uint256 totalEquipos=5;
    address owner;
  
    //creamos una estrutura votantes
    struct Votante
    {
        uint256 votosEmitidos;
         //creamos un mapa para registrar por quien voto s y cuantos votos le dio
        mapping (address => uint256) votosXAddress;
    }
    //creamos un mapa de votantes
    mapping (address => Votante) public votantes;
   
    //creamos un mapa de votantes y sus votos emitidos
   // mapping (address => uint256) public votantes;
    
    
    //creamos estructura para el equipo
    struct Equipo
    {
        uint256 votosRecibidos;
        string nombreEquipo;
        bool alta;
    }
    //creamos una mapa de equipos
    mapping (address => Equipo) public equipos;
    
    constructor() public
    {
        owner = msg.sender;
    }

//modificadores
    //no deje votar por un equipo que no este registrado
    //cada votante solo tiene 100 votos
    //solo se pueden dar de alta 5 equipos
    modifier totalEquiposPermitidos()
    {
        require(totalEquipos>0,"se supero la cantidad total de equipos");
        _;
    }
    //revisamos que no se hayan superado los 500 votos que existen
    modifier totalVotosPermitidos()
    {
        require(totalVotos>0,"se supero la cantidad total de votos");
        _;
    }
        
    
// votar
    function votar(address _equipo) public totalVotosPermitidos() returns (bool)
    {
        //revisamos si ya esta dado de alta el equipo
        Equipo storage miEquipo=equipos[_equipo];
        require(miEquipo.alta,"equipo no dado de alta");
        //revisamos que aun tenga votos para emitir
        Votante storage persona=votantes[msg.sender];
        require(persona.votosEmitidos < 100, "superaste tu limite de 100 votos");
        
        //damos el alta del votos
        miEquipo.votosRecibidos+=1;
        persona.votosEmitidos+=1;
        //descontamos el total de votosEmitidos
        totalVotos -=1;
        //registramos a quien le dio el voto
        persona.votosXAddress[_equipo]+=1;
        return true;
    }
// agregar un candidato (dar de alta X equipos si X es menor a 5) <----deberia ser equipo en vez de candidato
    function altaEquipo(address _equipo, string _nombreEquipo) public totalEquiposPermitidos() returns (string)
    {
        //revisamos si ya esta dado de alta el equipo
        Equipo storage miEquipo=equipos[_equipo];
        require(!miEquipo.alta,"equipo ya dado de alta");
        //Damos el alta
        miEquipo.nombreEquipo=_nombreEquipo;
        miEquipo.alta=true;
        //bajamos el total de equipos
        totalEquipos -=1;
        return _nombreEquipo;
    }
// ver cuantos votos tiene el equipo
    function votosXequipo(address _equipo) view public returns (uint256)
    {
        Equipo storage miEquipo=equipos[_equipo];
        return miEquipo.votosRecibidos;
    }
// ver por quien he votado y cuantos votos le di a determinado equipo
    function votosDados(address _equipo) view public returns (uint256)
    {
         Votante storage persona=votantes[msg.sender];
         return persona.votosXAddress[_equipo];
    }
}
