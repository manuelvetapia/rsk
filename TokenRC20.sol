pragma solidity 0.4.24;

contract tokenERC20{
    //Nombre
    string public name = 'VTMCoinReloaded';
    //Simbolo
    string public symbol = 'VTMC';
    
    constructor(uint256 _initialSupply) public{
        balanceOf[msg.sender] = _initialSupply;
        totalSupply = _initialSupply;
    }
    
    // EVENTOS
    // Transfer
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    
    // Approval
    event Approval(address indexed _from, address indexed _to, uint256 _value);
    
    
    // totalSupply
    uint256 public totalSupply;
    
    // balanceOf
    mapping(address => uint256) public balanceOf;
    
    // allowance
    mapping(address => mapping(address => uint256)) public allowance;
    // LEO => (Marco => 20)
    
    // transfer
    function transfer(address _to, uint256 _value) public returns (bool){
        require(balanceOf[msg.sender] >= _value);
        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;
        
        //ejecutamos el evento que rastrea esta transaccion
        emit Transfer(msg.sender, _to, _value);
        return true;
    }
    // transferFrom
    function transferFrom(address _from, address _to, uint256 _value) public returns(bool){
        require(_value <= balanceOf[_from]);
        require(_value <= allowance[_from][msg.sender]);
        
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        // emitimos el evento para rastrear la transferencia
        emit Transfer(_from, _to, _value);
        return true;
    }
    // approve
    
    function approve(address _spender, uint256 _value) public returns(bool){
        allowance[msg.sender][_spender] = _value;
        
        //ejecutamos un evento para rastrear la aprobacion
        emit Approval(msg.sender, _spender, _value);
        
        return true;
    }
    
}

