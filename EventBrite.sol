// version de solidity
pragma solidity ^0.4.24;

contract Eventbrite{
    //declaro las variables
        // total de tickets que voy a liberar
        uint tickets;
        // descripcion del evento
        string description;
        // precio por ticket
        uint price = 1 ether;
        // comprador => tickets
        mapping(address => uint) public purchasers;
        // dueno del contrato
        address owner;
        address contrato = this;
    //declaro el constructor
    constructor(uint _tickets, string _description) public{
       tickets = _tickets; 
       description = _description;
       owner = msg.sender;
    }
    
    // Modificadores
    modifier isOwner(){
        require(owner == msg.sender);
        _;
    }
    
    //declaro las funciones
      // comprar ticket
      function buyTicket(uint _amount) public payable{
          //validar que lo que mando es suficiente
          require(msg.value >= price*_amount);
          //que todavia tenga tickets disponibles
          require(_amount <= tickets);
          //si paga de mas regresarle el cambio
          if(msg.value > (_amount*price)){
              msg.sender.transfer(msg.value-(_amount*price));
          }
          
          //actualizar saldos
          purchasers[msg.sender] += _amount;
          tickets -= _amount;
      }
      // consultar evento
      function infoEvent() view public returns(address, string, uint, uint){
          return (owner, description, tickets, price);
      }
      // reembolsar ticket
      function refundTicket(uint _ticketsRefunded) public payable{
          if(purchasers[msg.sender] <  _ticketsRefunded){
              revert();
          }
          msg.sender.transfer(_ticketsRefunded * price);
          
          //actualizamos saldos
          tickets += _ticketsRefunded;
          purchasers[msg.sender] -= _ticketsRefunded;
      }
      
      // retirar fondos hacia el dueno
      
      function cashOut() public payable isOwner(){
          owner.transfer(contrato.balance);
      }
}
