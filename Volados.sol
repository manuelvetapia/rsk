pragma solidity ^0.4.24;
contract Volados
{
    //dueño del conttrato
    address owner;
    //direccion de este Contrato
    address contrato = this;
    //contador del numero del Evento 
    uint256 numeroEvento;
    //costo x ticket
    uint costoTicket=100 wei;
  
    //creamos estructura para cada apuesta
    struct Apostar
    {
        //tickets comprados
        uint256 tickets;
        //apuesta par o non
        bool par;
        //cobro ganacias
        bool cobroGanacias;
    }
    //creamos una estructura para cada evento
    struct Evento
    {
    //datos del evento
        string nombreEvento;
        string fechaEvento;
        //resultado de ese evento ya sea par o non
        bool resultadoPar;
        //si el evento esta activo o ya termino el sorteo
        bool activo;
        //mapa de apostadores con el monto invertido y el resultado por el que apuestan
        mapping (address => Apostar) apostadores;
        //monto total que han invertido los apostadores
        uint256 apuestaTotalAcumulada;
        //cuantos tickest para par
        uint256 ticketsPar;
        //cuantos tickets para non
        uint256 ticketsNon;
        //ver si ya cobro la comision el owner
        bool cobroComision;
        //cuanto pagar por ganadores
        uint256 repartidoPaga;
    }
    
    //mapa de todos los eventos creados, empiezan con el id 1
    mapping (uint256 => Evento) public eventos;
    
    constructor() public
    {
        owner = msg.sender;
        //inicializamos los eventos
        numeroEvento=0;
    }
    
    //Solo owner puede crear Evento, cobrar comision, poner resultados
    modifier isOwner()
    {
         require(msg.sender==owner, "solo el owner puede operar esta funcion");
         _;
    }



    //Crear Evento
    function crearEvento(string _evento, string _fechaEvento) public isOwner()
    {
        require(msg.sender==owner, "solo puede crear eventos el owner");
        numeroEvento+=1;
        Evento storage miEvento=eventos[numeroEvento];
        miEvento.nombreEvento=_evento;
        miEvento.fechaEvento=_fechaEvento;
        miEvento.activo=true;
        
    }
    //Poner resultados de evento
    function resultadosEvento(uint256 _idEvento, bool _resultadoEvento) public isOwner()
    {
        Evento storage miEvento=eventos[_idEvento];
        require(miEvento.activo==true,"el evento ya concluyo no puedes volver a poner resultados");
        miEvento.activo=false;
        miEvento.resultadoPar=_resultadoEvento;
        //invocamos la funcion para cobrar la comision
        cobraComision(_idEvento);
    }
    
    //Cobrar comision (owner)
    function cobraComision(uint256 _idEvento) payable public isOwner()
    {
        //metemos los eventos a storage para consultarlos
        Evento storage miEvento=eventos[_idEvento];
        //revisamos que el evento haya terminado
        require(miEvento.activo==false,"el evento aun esta activo");
        //revisamos que haya ganacias
        require(miEvento.apuestaTotalAcumulada>=100,"no hay weis suficientes por cobrar");
        //revisamos que no haya cobrado la comision
        require(miEvento.cobroComision==false,"ya cobraste tu comision, ambicioso :)");
        //vemos cuantos tickets ganadores hay
            //gano par
        if(miEvento.resultadoPar)
        {
            //quiere decir que alguien si gano y solo cobramos nuestra comision
            if(miEvento.ticketsPar>0)
            {
                owner.transfer(miEvento.apuestaTotalAcumulada/100);
                //actualizamos cuanto dinero hay
                miEvento.apuestaTotalAcumulada-=(miEvento.apuestaTotalAcumulada/100);
                //hacemos la cuenta de cuanto le toca a cada ganador
                miEvento.repartidoPaga=miEvento.apuestaTotalAcumulada/miEvento.ticketsPar;
            }//quiere decir que nadie gano y cobramos todo 
            else
            {
                owner.transfer(miEvento.apuestaTotalAcumulada);
                //actualizamos cuanto dinero hay
                miEvento.apuestaTotalAcumulada-=miEvento.apuestaTotalAcumulada;
            }
        }//gano non
        else
        {
            //quiere decir que alguien si gano y solo cobramos nuestra comision
            if(miEvento.ticketsNon>0)
            {
                owner.transfer(miEvento.apuestaTotalAcumulada/100);
                //actualizamos cuanto dinero hay
                miEvento.apuestaTotalAcumulada-=(miEvento.apuestaTotalAcumulada/100);
                //hacemos la cuenta de cuanto le toca a cada ganador
                miEvento.repartidoPaga=miEvento.apuestaTotalAcumulada/miEvento.ticketsNon;
            }//quiere decir que nadie gano y cobramos todo 
            else
            {
                owner.transfer(miEvento.apuestaTotalAcumulada);
                //actualizamos cuanto dinero hay
                miEvento.apuestaTotalAcumulada-=miEvento.apuestaTotalAcumulada;
            }
        }
        miEvento.cobroComision=true;
    }
    //Apostar en Evento
    //declarar payable ya le descuenta lo que tenga en value al momento de ejecutar la funcion al sender
    function Apuesta (uint256 _tickets, uint256 _idEvento, bool _par) public payable 
    {
        //revisamos que el owner no apueste
        require(msg.sender != owner, "el owner no puede jugar volados");
        //revisamos que ya exista el evento
        require(_idEvento<=numeroEvento&&_idEvento>0,"el evento no existe");
        //metemos los eventos a storage para consultarlos
        Evento storage miEvento=eventos[_idEvento];
        //revisamos que el evento aun este activo
        require(miEvento.activo==true,"el evento no esta activo");
        //creamos el apostador y lo metemos a storage
        Apostar storage miApostador = miEvento.apostadores[msg.sender];
        //revisamos que no haya apostado en este evento
        require(miApostador.tickets<1,"ya apostaste en este evento.");
        //revisamos que tenga saldo suficiente para la apuesta
        require(msg.value>=_tickets*costoTicket,"no tienes saldo suficiente para apostar");
        //le depositamos su cambio
        if(msg.value>_tickets*costoTicket)
        {
            msg.sender.transfer(msg.value-(_tickets*costoTicket));
        }
        //registramos cuanto aposto
        miApostador.tickets=_tickets;
        //registramos a que resultado le aposto
        miApostador.par=_par;
        //actualizamos el total de apuestas
        miEvento.apuestaTotalAcumulada+=miApostador.tickets*costoTicket;
        //actualizamos el conteo de tickets par o non
        if(miApostador.par)
        {
            miEvento.ticketsPar+=_tickets;
        }else
        {
             miEvento.ticketsNon+=_tickets;
        }
    }
     //Cobra apuesta quien aposto y gano, ya concluido el evento
    function CobrarApuesta(uint256 _idEvento) payable public
    {
        //revisamos que no sea el owner quien quiera cobrar
        require(msg.sender != owner, "el owner no cobrar");
        //metemos los eventos a storage para consultarlos
        Evento storage miEvento=eventos[_idEvento];
        //creamos el apostador y lo metemos a storage
        Apostar storage miApostador = miEvento.apostadores[msg.sender];
        //revisamos que el evento no este activo
        require(miEvento.activo==false,"el evento aun esta activo");
        //revisamos que haya apostado en este evento
        require(miApostador.tickets>=1,"no apostaste en este evento.");
        //revisamos que no haya cobrado
        require(miApostador.cobroGanacias==false,"ya cobraste tus ganacias");
        //revisamos que haya ganadores
        require(miApostador.par==miEvento.resultadoPar,"no resultaste ganador suerte la proxima");

        //pagamos apuesta
        //hacemos la transferencia de su parte
        msg.sender.transfer(miEvento.repartidoPaga*miApostador.tickets);
        //actualizamos el total de la apuesta
        miEvento.apuestaTotalAcumulada-=miEvento.repartidoPaga*miApostador.tickets;
        //actualizamos que ya cobro
        miApostador.cobroGanacias=true;
    }
    
     //regresa los datos del evento que consulta el sender 
     function verMiApuestaEnEvento(uint256 _idEvento) view public returns (uint256, bool, bool, uint256)
     {
         Evento storage miEvento=eventos[_idEvento];
         Apostar storage miApostador = miEvento.apostadores[msg.sender];
         return (miApostador.tickets, miApostador.par, miApostador.cobroGanacias, contrato.balance);
     }
}